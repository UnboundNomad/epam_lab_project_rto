package com.epam.lab.rto.dto;

public enum RequestStatus {

    ACTIVE, REJECTED, CANCELED;

}
