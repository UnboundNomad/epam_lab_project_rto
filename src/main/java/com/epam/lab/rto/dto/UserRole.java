package com.epam.lab.rto.dto;

public enum UserRole {

    ADMIN, USER;
}
