<%@page pageEncoding="UTF-8"%>

<body>
	<header>
		<div class="container">
			<div class="clearfix" id="heading">
				<img src="/img/logo_white.png" alt="logo" class="logo">
				<nav>
					<ul class="menu">
						<li>
							<a class="btn" href="/admin/requests">
								<span>Заявки</span>
							</a>
						</li>
						<li>
							<a class="btn" href="/schedule">
								<span>Расписание</span>
							</a>
						</li>
						<li>
							<a class="btn" href="/admin/route">
								<span>Маршруты</span>
							</a>
						</li>
						<li>
							<a class="btn" href="/personal-area">
								<span>Личный кабинет</span>
							</a>
						</li>
						<li>
							<a class="btn" href="/find-train">
								<span>Заказать билет</span>
							</a>
						</li>
						<li>
							<a class="btn" href="/logout">
								<span>Выйти</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div id="title_first">
				<h1>трансгалактическая <br> железная дорога*</h1>
				<p id="addition">*работает только на территории Российской Федерации</p>
			</div>
		</div>
		<div class="separator"></div>
	</header>