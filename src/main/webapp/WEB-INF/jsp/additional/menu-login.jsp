<%@page pageEncoding="UTF-8"%>

<body>
	<header>
		<div class="container">
			<div class="clearfix" id="heading">
				<img src="/img/logo_white.png" alt="logo" class="logo">
				<nav>
					<ul class="menu">
						<li>
							<a class="btn" href="/login">
								<span>Вход</span>
							</a>
						</li>
						<li>
							<a class="btn" href="/login/registration">
								<span>Регистрация</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div id="title_first">
				<h1>трансгалактическая <br> железная дорога*</h1>
				<p id="addition">*работает только на территории Российской Федерации</p>
			</div>
		</div>
		<div class="separator"></div>
	</header>